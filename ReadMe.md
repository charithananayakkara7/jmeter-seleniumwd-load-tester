# Jmeter Selenium Web Driver Load Tester

[![Repo Status](https://www.repostatus.org/badges/latest/active.svg)]()
[![Build Status](https://camo.githubusercontent.com/80a3fec8ea098e04112a6ae287b515a104f55b18/68747470733a2f2f6170702e776572636b65722e636f6d2f7374617475732f39653062633534633433663438643232306165633638346666666232623131302f732f6d6173746572)]()

This repository contains a simple Load Testing using JMeter and Selenium Web Driver. It has the capability to execute multiple threads(hits to the server) and test the behaviour of the application under a heavy load.
Below are the technologies which have used to implement this.

  - Selenium Web Driver JS
  - Jmeter
  
# New Features!

  - This framework has an integration with JMeter and Selenium.
  - User can Monitor the Application Behaviour under a havy load.


You can also:
  - Implement Multiple Browser Capability.
  - Implement Multiple Application flow Scenario executions.

### Tech

Jmeter Selenium Load Testing Framework uses a number of open source projects to work properly:

* [Selenium Web Driver JS] - To interact with selenium and the application.
* [Jmeter] - Multi Threaded executions.
* [Chrome Driver] - Chrome Browser.
* [Gheko Driver] - Firefox Browser.

And of course This framework itself is open source with a [public repository](https://gitlab.com/erandakodagoda/jmeter-seleniumwd-load-tester)
 on GitLab.

### Installation

This Framework requires [Selenium Web Driver](https://selenium.dev/) v3+ and [Apache JMeter](https://jmeter.apache.org/) to run.

Install the dependencies and devDependencies and start the server.

```
$ Open the .jmx file using Jmeter.
$ Run the project By changing the thread count in the thread group.
```

### Development

Want to contribute? Great!

This framework uses Selenium Web Driver JS + JMeter for fast developing.
Make a change in your file and instantaneously see your updates!

### Todos

 - Write MORE Tests
 - Implement Listeners to get More Stats

License
----
MIT

Development
----
* Initial Development - [Eranda Kodagoda](https://www.linkedin.com/in/erandakodagoda/)

**Free Software**
